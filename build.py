#!/usr/bin/env python3
import os
import sys
import argparse

OS_TYPES = ["centos", "ubuntu"]
TAG_FORMAT = "viert/buildmachine-{0}-{1}:{2}"
struct = {}


def build_image(os_type, version, arch):
    print(f"===== Building image for {os_type}:{version}/{arch} =====")
    prevdir = os.path.abspath(os.curdir)
    print(prevdir)
    dockerfile = struct[os_type][version][arch]
    dockerdir = os.path.dirname(dockerfile)
    dockerfile = os.path.basename(dockerfile)
    tag = TAG_FORMAT.format(os_type, arch, version)
    os.chdir(dockerdir)
    cmd = f"docker build -t {tag} -f {dockerfile} ."
    retcode = os.system(cmd)
    os.chdir(prevdir)
    if retcode != 0:
        raise RuntimeError("Error building image")
    cmd = f"docker push {tag}"
    os.system(cmd)


def build_all():
    for os_type in struct:
        for version in struct[os_type]:
            for arch in struct[os_type][version]:
                build_image(os_type, version, arch)


def build_structure():
    global struct
    for os_type in OS_TYPES:
        struct[os_type] = {}
        for version in os.listdir(os_type):
            struct[os_type][version] = {}
            for arch in os.listdir(os.path.join(os_type, version)):
                struct[os_type][version][arch] = os.path.join(
                    os_type, version, arch, 'Dockerfile')
    return struct


def main():
    if len(sys.argv) > 1 and len(sys.argv) < 4:
        print("Usage: build.py [<os_type> <version> <arch>]")
        raise SystemExit(1)
    struct = build_structure()

    if len(sys.argv) > 1:
        os_type, version, arch = sys.argv[1:4]
        if os_type not in struct:
            raise ValueError("invalid os_type %s" % os_type)
        if version not in struct[os_type]:
            raise ValueError("invalid version %s for os_type %s" %
                             (version, os_type))
        if arch not in struct[os_type][version]:
            raise ValueError("invalid arch %s for os_type %s and version %s" % (
                arch, os_type, version))
        os.system("docker login")
        build_image(os_type, version, arch)
    else:
        os.system("docker login")
        build_all()


if __name__ == '__main__':
    main()
