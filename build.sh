#!/bin/bash

VALID_TYPES="centos_7_x64 centos_6_x64 centos_6_i386"

reqtype=$1
found=0
if [ "" != "$reqtype" ]; then
    for vtype in ${VALID_TYPES}; do
        if [ $reqtype == $vtype ]; then
            found=1
            break
        fi
    done
    if [ $found -eq 1 ]; then
        VALID_TYPES=$reqtype
    else
        echo "type $reqtype not found"
        exit 1
    fi
fi


docker login

for basever in 5 6 7; do
    for arch in x64 i386; do
        type="${basever}_${arch}"
        for vtype in ${VALID_TYPES}; do
            if [ $type == $vtype ]; then
                echo "================ CENTOS $basever:$arch =================="
                pushd centos/$basever
                docker build -t viert/buildmachine-centos-$arch:$basever -f Dockerfile.buildmachine-centos$basever-$arch .
                popd
                docker push viert/buildmachine-centos-$arch:$basever
            fi
        done
    done
done
